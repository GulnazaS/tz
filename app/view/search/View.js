Ext.define('Sample.view.search.View', {
    extend: 'Ext.form.Panel',
    xtype: 'app-search',
    title: 'Условия поиска',
    layout: {
        type: 'form'
    },

    controller: 'app-search',
    defaults:{
        labelAlign: 'top',
        flex: 1,
    },
    items:[{
        xtype: 'textfield',
        fieldLabel: 'Фамилия',
        name: 'firstname',
    }, {
        xtype: 'textfield',
        fieldLabel: 'Имя',
        name: 'lastname',
    },
    {
        xtype: 'textfield',
        fieldLabel: 'Отчество',
        name: 'fullname',
    },
    {
        xtype: 'datefield',
        fieldLabel: 'Начало периода даты рождения',
        name: 'bdayfist',
    },
    {
        xtype: 'datefield',
        fieldLabel: 'Конец периода даты рождения',
        name: 'bdaylast',
    },
    ],
    dockedItems: [{
        xtype:'toolbar',
        dock: 'bottom',
        items:[
            {
            text:'Поиск',
            handler: 'onSearch'
            },
            {
                text: 'Выход',
                handler: 'onClose'
            }
        ]
        
    }] 
    
});
