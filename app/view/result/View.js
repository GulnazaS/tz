Ext.define('Sample.view.result.View', {
    extend: 'Ext.window.Window',
    xtype: 'app-result',
    title: 'Анкета гражданина',

    config:{
        record: null
    },
    controller: 'app-result',
    width: 600,
    height: 500,
    modal: true,
    autoShow: true,
    margin: 30,
    items: [{
        xtype: 'form',
        reference:'form',
        bodyPadding:12,
        defaults:{
            labelAlign: 'left',
            flex: 1,
        },
        items: [{
                xtype: 'textfield',
                fieldLabel: 'Фамилия',
                name: 'firstname',
            }, {
                xtype: 'textfield',
                fieldLabel: 'Имя',
                name: 'lastname',
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Отчество',
                name: 'fullname',
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Дата рождения',
                name: 'bday',
            },
        ],
    }],
    
    buttons: [{
        text: 'Выход',
        handler: 'onClose'
    }, ],


});