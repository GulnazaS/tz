Ext.define('Sample.view.result.ViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.app-result',

    onClose: function () {
        const me = this;
        const form = me.lookup('form');
        const view = me.getView();

        if (form.isDirty()) {
            Ext.Msg.show({
                title: 'Сохранить изменения?',
                message: 'Точно сохранить?',
                buttons: Ext.Msg.YESNO,
                icon: Ext.Msg.QUESTION,
                fn: function (btn) {

                    if (btn === 'yes') {
                        const record = form.getRecord();
                        const values = form.getValues();

                        view.setLoading(true);
                        record.set(values);

                        record.save({
                            callback: function (record, operation, success) {
                                if (success) {
                                    Sample.Utils.syncAnketas(record);
                                    view.close();
                                } else {
                                    view.setLoading(false);
                                }

                            }
                        })
                    } else {
                        view.close();
                    }
                }
            });
        } else {
            view.close();
        }
    },
    init: function (view) {
        const me = this;
        const ret = me.callParent([view]);
        const form = me.lookup('form');
        const record = view.getRecord();

        form.loadRecord(record);
        form.query('field').map(x => x.resetOriginalValue());
        return ret;
    },
});