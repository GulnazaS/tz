Ext.define('Sample.view.window.anketas.View', {
    extend: 'Ext.window.Window',
    xtype: 'app-anketas',
    title: 'Результаты поиска',
    viewModel: {
        stores: {
            Anketas: {
                type: 'chained',
                source: 'Anketas'
            }
        }
    },
    controller: 'app-anketas',
    height: 400,
    width: 600,
    layout: 'fit',
    autoShow: true,
    items: [{
        xtype: 'grid',
        reference: 'anketasGrid',
        selModel: {
            mode: 'SINGLE'
        },
        bind: {
            store: '{Anketas}'
        },

        columns: [{
                header: 'Фамилия',
                dataIndex: 'firstname'
            }, {
                header: 'Имя',
                dataIndex: 'lastname'
            },
            {
                header: 'Отчество',
                dataIndex: 'fullname'
            }, {
                header: 'Дата рождения',
                dataIndex: 'bday',
                xtype: 'datecolumn',
                format: 'd/m/Y',
                flex: 1
            },
        ],

    }],
    buttons: [{
            text: 'Добавить',
            handler: 'add'
        }, {
            text: 'Изменить',
            handler: 'edit',
            bind: {
                disabled: '{!anketasGrid.selection}'
            }
        },
        {
            text: 'Удалить',
            handler: 'delete',
            bind: {
                disabled: '{!anketasGrid.selection}'
            }
        },
        {
            text: 'Печать',
            handler: 'print',
            bind: {
                disabled: '{!anketasGrid.selection}'
            }
        },
        {
            text: 'Выход',
            handler: 'onClose',

        },
    ],
});