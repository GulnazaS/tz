Ext.define('Sample.view.window.anketas.ViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.app-anketas',

    onClose: function () {
        this.getView().close();
    },
    add: function () {
        Ext.create('Sample.view.result.View', {
            record: Ext.create('Sample.model.Anketa')
        });
    },
    edit: function () {
        const record = this.lookup('anketasGrid').getSelection();
        Ext.create('Sample.view.result.View', {
            record: record[0].copy()
        });
    },
    delete: function () {
        const me = this;
        const anketasGrid = me.lookup('anketasGrid');
        const record = anketasGrid.getSelection();
        const store = anketasGrid.getStore();

        store.remove(record);
    },
    print: function () {

    },

});