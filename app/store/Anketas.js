Ext.define('Sample.store.Anketas', {
    extend: 'Ext.data.Store',
    storeId: 'Anketas',
    alias: 'store.anketas',
    model: 'Sample.model.Anketa',
    requires: ['Sample.model.Anketa'],

    data: {
        items: [{
                id: 1,
                firstname: 'Jean Luc',
                lastname: "Jean Lu",
                fullname: "Jean Lu",
                bday: "01.01.1990"
            },
            {
                id: 2,
                firstname: 'Jefffan Luc',
                lastname: "Jffean Lu",
                fullname: "Jssean Lu",
                bday: "01.01.1990"
            },
            {
                id: 3,
                firstname: 'Jedddan Luc',
                lastname: "Jedddan Lu",
                fullname: "Jessan Lu",
                bday: "01.01.1990"
            },
        ]
    },

    proxy: {
        type: 'memory',
        
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});