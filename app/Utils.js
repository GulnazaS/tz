Ext.define('Sample.Utils', {
    singleton: true,
    syncAnketas: function (record) {
        const store = Ext.getStore('Anketas');
        const existRecord = store.getById(record.getId());

        if (existRecord) {
            existRecord.set(record.getData());
            existRecord.commit();
        }
    }

});