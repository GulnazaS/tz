Ext.define('Sample.model.Anketa', {
    extend: 'Ext.data.Model',
    requires: [
        'Ext.data.proxy.Memory',
    ],
    proxy: {
        type: 'memory',
    },

    fields: [{
            name: 'firstname',
            type: 'string',
        }, {
            name: 'lastname',
            type: 'string',
        },
        {
            name: 'fullname',
            type: 'string',
        },
        {
            name: 'bday',
            type: 'date',
        },
    ]
});