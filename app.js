/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: 'Sample.Application',

    name: 'Sample',
    stores: ['Anketas'],
    requires: [
        'Sample.*', 'Ext.*'
    ],

    // The name of the initial view to create.
    mainView: 'Sample.view.search.View'
});
